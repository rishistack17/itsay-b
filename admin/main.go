package main

import (
	"encoding/json"
	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
    ok := make(map[string]interface{})
	ok["res"] =1
	json.NewEncoder(w).Encode(ok)
}

func main() {
    http.HandleFunc("/", handler)
    http.ListenAndServe(":8000", nil)
}
