package auth

import (
	// "os"
	"fmt"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/joho/godotenv"
	"github.com/rishi-org-stack/ItsaboutYou-b/model"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type TokenCred struct {
	ID      primitive.ObjectID `bson:"_id,omitempty"  `
	Token   string             `bson:"token"  json:"token"`

}

var db = model.Instantiate().Connect().CreateDb("ITSABOUTYOU").LinkToCollection("tokens")

func VerifyToken(tokenString string) (*jwt.Token, error) {
	var tk = &TokenCred{

	}
	toeknisunvalid:=db.Get("token",tokenString,tk)
	if toeknisunvalid!=nil{
		return nil,toeknisunvalid
	}
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		//Make sure that the token method conform to "SigningMethodHMAC"
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(os.Getenv("ACCESS_SECRET")), nil
	})
	if err != nil {
		return nil, err
	}
	return token, nil
}

func TokenValid(s string) error {
	token, err := VerifyToken(s)
	if err != nil {
		return err
	}
	if _, ok := token.Claims.(jwt.Claims); !ok && !token.Valid {
		return err
	}
	return nil
}
func ExtractPayload(j *jwt.Token) jwt.MapClaims {
	return j.Claims.(jwt.MapClaims)
}
func CreateToken(id string, password string) (string, error) {
	var err error
	godotenv.Load(".env")
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["user_id"] = id
	atClaims["password"] = password
	atClaims["exp"] = time.Now().Add(time.Hour * 168).Unix()
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	token, err := at.SignedString([]byte(os.Getenv("ACCESS_SECRET")))
	if err != nil {
		return "", err
	}
	tk:=TokenCred{}
	tk.Token=token
	db.Insert(tk)
	return token, nil
}
