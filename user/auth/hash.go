package auth

import (
	// "bytes"
	"fmt"

	"golang.org/x/crypto/bcrypt"
)
	 

func GenrateHash(pas string)string{
	pass := []byte(pas)
	pass,err:=bcrypt.GenerateFromPassword(pass,bcrypt.DefaultCost)
	if err!=nil{
		fmt.Printf("err in hashing :\t%v\n",err)
		return ""
	}
	return string(pass)
}

func CompareHashes(hpass []byte, pass []byte)bool{
	// pass2 := []byte(pass)
	err:=bcrypt.CompareHashAndPassword(hpass,pass)
	if err !=nil{
		fmt.Printf("err in comparing :\t%v\n",err)
		return false
	}
	return true
}