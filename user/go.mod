module github.com/rishi-org-stack/ItsaboutYou-b

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/sessions v1.2.1
	github.com/joho/godotenv v1.3.0
	github.com/rs/cors v1.7.0
	github.com/stretchr/testify v1.7.0 // direct
	go.mongodb.org/mongo-driver v1.5.0
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2
	// golang.org/x/tools v0.0.0-20190531172133-b3315ee88b7d // indirect
)
