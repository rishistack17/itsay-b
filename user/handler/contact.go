package handler

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rishi-org-stack/ItsaboutYou-b/model"
	"go.mongodb.org/mongo-driver/bson/primitive"
	// "go.mongodb.org/mongo-driver/bson/primitive"
)

type contactRequest struct {
	Contact model.Contact `json:"contact"`
	User    string        `json:"user,omitempty"`
}

func AddConact(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-type", "application/json")
	db.LinkToCollection("user")
	var cReq contactRequest
	var u model.User
	json.NewDecoder(r.Body).Decode(&cReq)
	db.Get("email", cReq.User, &u)
	cReq.Contact.User_ID = u.ID
	db.LinkToCollection("contact")
	id := db.Insert(cReq.Contact)
	db.LinkToCollection("user")
	find := make(map[string]interface{})
	find["identity"] = "_id"
	find["val"] = u.ID
	val := make(map[string]interface{})
	if len(u.Contacts) > 0 {

		val["contacts"] = id.(primitive.ObjectID)

		err := db.AddaDocument(find, val)
		if err != nil {
			fmt.Println(err)
		}
	} else {
		u.Contacts = make([]primitive.ObjectID, 0)
		u.Contacts = append(u.Contacts, id.(primitive.ObjectID))

		val["contacts"] = u.Contacts
		db.UpdateaDocument(find, val)
	}
	res.Success("Contact updated", u.Contacts)
	json.NewEncoder(w).Encode(res)
}

func GetAllContact(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-type", "application/json")
	db.LinkToCollection("user")
	var u model.User
	vars:=mux.Vars(r)
	id,_:=primitive.ObjectIDFromHex(vars["id"])
	db.Get("_id",id, &u)
	contact := db.GetAllContactsofUser(&u)
	fmt.Println(contact)
	res.Success("Get all contact is success full", contact)
	json.NewEncoder(w).Encode(res)
}

func UpdateAfieldofContact(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-type", "application/json")
	db.LinkToCollection("contact")
	var q Query
	json.NewDecoder(r.Body).Decode(&q)
	vars := mux.Vars(r)
	id, _ := primitive.ObjectIDFromHex(vars["id"])
	find := make(map[string]interface{})
	find["identity"] = "_id"
	find["val"] = id
	_, err := db.UpdateaDocument(find, q.Val.(map[string]interface{}))
	if err != nil {
		res.ServerError("Error in Updating:"+err.Error(), nil)

	} else {

		res.Success("Get all contact is success full", nil)
	}
	json.NewEncoder(w).Encode(res)
}

//TODO: poping a  paricular element from contacts array is very costly emprove it
func DeleteContact(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-type", "application/json")
	var u model.User
	var q model.Contact
	json.NewDecoder(r.Body).Decode(&q)
	session, _ := store.Get(r, "session")
	db.Get("email", session.Values["email"], &u)
	find := make(map[string]interface{})
	val := make(map[string]interface{})
	find["identity"] = "_id"
	find["val"] = u.ID
	// delete(u.Contacts,q.ID.Hex())
	val["tasks"] = u.Tasks
	fmt.Println(u)
	db.UpdateaDocument(find, val)
	db.LinkToCollection("contact")
	find2 := make(map[string]interface{})
	find2["identity"] = "_id"
	find2["val"] = q.ID

	_, err := db.DeleteADocument(find2)
	if err != nil {
		res.ServerError("Error in deleting:"+err.Error(), nil)
	} else {
		res.Success("Deleted syccessfully", find2)
	}
	json.NewEncoder(w).Encode(res)

}
