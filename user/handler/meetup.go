package handler

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rishi-org-stack/ItsaboutYou-b/model"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type meetupRequest struct {
	Meetup model.Meetup `json:"meetup"`
	User   string       `json:"user"`
}

//TODO test thes routes and commit
func AddMeetup(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-type", "application/json")
	db.LinkToCollection("user")
	var meetup meetupRequest
	var u model.User
	json.NewDecoder(r.Body).Decode(&meetup)
	db.Get("email", meetup.User, &u)
	meetup.Meetup.User_ID = u.ID
	db.LinkToCollection("meetup")
	id := db.Insert(meetup.Meetup)
	db.LinkToCollection("user")
	find := make(map[string]interface{})
	find["identity"] = "_id"
	find["val"] = u.ID
	fmt.Println(u.ID)
	val := make(map[string]interface{})
	if len(u.Meetups) > 0 {

		val["meetups"] = id.(primitive.ObjectID)

		err := db.AddaDocument(find, val)
		if err != nil {
			fmt.Println(err)
		}
		res.Success("successfully added meettup", meetup)
	} else {
		u.Meetups = make([]primitive.ObjectID, 0)
		u.Meetups = append(u.Meetups, id.(primitive.ObjectID))

		val["meetups"] = u.Meetups
		db.UpdateaDocument(find, val)
		res.Success("successfully added meettup", meetup)
	}
	json.NewEncoder(w).Encode(res)
}

func GetAllmeetup(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-type", "application/json")
	db.LinkToCollection("user")
	var u model.User
	vars := mux.Vars(r)
	id, _ := primitive.ObjectIDFromHex(vars["id"])
	db.Get("_id", id, &u)

	contact := db.GetAllmeetupsofUser(&u)
	fmt.Println(contact)
	res.Success("Get all contact is success full", contact)
	json.NewEncoder(w).Encode(res)
}

func UpdateAfieldofmeetup(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-type", "application/json")
	db.LinkToCollection("meetup")
	var q Query
	json.NewDecoder(r.Body).Decode(&q)
	vars := mux.Vars(r)
	id, _ := primitive.ObjectIDFromHex(vars["id"])
	find := make(map[string]interface{})
	find["identity"] = "_id"
	find["val"] = id
	_, err := db.UpdateaDocument(find, q.Val.(map[string]interface{}))
	if err != nil {
		res.ServerError("Error in Updating:"+err.Error(), nil)

	} else {

		res.Success("Get all contact is success full", nil)
	}
	json.NewEncoder(w).Encode(res)
}
