package handler

import (
	"encoding/json"
	"fmt"

	// "fmt"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rishi-org-stack/ItsaboutYou-b/model"
	"go.mongodb.org/mongo-driver/bson/primitive"
	// "go.mongodb.org/mongo-driver/bson/primitive"
)

//TODO: work on sssion in storing user email
type taskRequest struct {
	Task model.Task `json:"task"`
	User string     `json:"user"`
}

func AddTask(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-type", "application/json")
	var body taskRequest
	var u model.User
	vars:=mux.Vars(r)
	uid,_:=primitive.ObjectIDFromHex(vars["id"])
	json.NewDecoder(r.Body).Decode(&body)
	db.Get("_id",uid, &u)
	body.Task.User_ID = u.ID
	db.LinkToCollection("task")
	id := db.Insert(body.Task) 
	db.LinkToCollection("user")
	if len(u.Tasks) == 0 {
		u.Tasks = make(map[string]bool)
	}
	u.Tasks[id.(primitive.ObjectID).Hex()] = false
	find := make(map[string]interface{})
	find["identity"] = "_id"
	find["val"] = u.ID
	val := make(map[string]interface{})
	val["tasks"] = u.Tasks
	db.UpdateaDocument(find, val)
	res.Success("succesfully added  a task", body)
	json.NewEncoder(w).Encode(res)
}

func GetAllTask(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-type", "application/json")
	vars :=mux.Vars(r)
	uid,_:=primitive.ObjectIDFromHex(vars["id"])
	db.LinkToCollection("user")
	var u model.User
	db.Get("_id",uid ,&u)
	tasks := db.GetAllTaskofUser(&u)
	json.NewEncoder(w).Encode(tasks)
}

func MarkCompleted(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-type", "application/json")
	db.LinkToCollection("user")
	var u model.User
	var q model.Task
	json.NewDecoder(r.Body).Decode(&q)
	session, _ := store.Get(r, "session")
	db.Get("email", session.Values["email"], &u)
	fmt.Println(u)
	db.LinkToCollection("task")
	db.Get("subject", q.Subject, &q)
	find := make(map[string]interface{})
	val := make(map[string]interface{})
	find["identity"] = "_id"
	find["val"] = u.ID
	u.Tasks[q.ID.Hex()] = true
	val["tasks"] = u.Tasks
	db.LinkToCollection("user")
	_, err := db.UpdateaDocument(find, val)
	if err != nil {
		res.ServerError("Marked unsuccessfully  with error:"+err.Error(), nil)
	}
	res.Success("updated successfully", q.ID.Hex())
	json.NewEncoder(w).Encode(res)

}
func UpdateAfield(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-type", "application/json")
	db.LinkToCollection("task")
	var q Query
	json.NewDecoder(r.Body).Decode(&q)
	_, err := db.UpdateaDocument(q.Where.(map[string]interface{}), q.Val.(map[string]interface{}))
	if err != nil {
		res.ServerError("Updated unsuccessfully  with error:"+err.Error(), nil)
	}
	res.Success("updated successfully", q)
	json.NewEncoder(w).Encode(res)

}

func DeleteTask(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-type", "application/json")
	var u model.User
	var q model.Task
	json.NewDecoder(r.Body).Decode(&q)
	session, _ := store.Get(r, "session")
	db.Get("email", session.Values["email"], &u)
	find := make(map[string]interface{})
	val := make(map[string]interface{})
	find["identity"] = "_id"
	find["val"] = u.ID
	delete(u.Tasks, q.ID.Hex())
	val["tasks"] = u.Tasks
	fmt.Println(u)
	db.UpdateaDocument(find, val)
	db.LinkToCollection("task")
	find2 := make(map[string]interface{})
	find2["identity"] = "_id"
	find2["val"] = q.ID

	_, err := db.DeleteADocument(find2)
	if err != nil {
		res.ServerError("Deleted unsuccessfully  with error:"+err.Error(), nil)
	}
	res.Success("deleted successfully", q)
	json.NewEncoder(w).Encode(res)
}
