package handler

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rishi-org-stack/ItsaboutYou-b/model"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type timelineRequest struct {
	Timeline model.Timeline `json:"timeline"`
	User    string        `json:"user"`
}

//TODO :THINK ABOUT TIMELINE NEEDS IMPROVEMNENT
func AddTimeline(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-type", "application/json")
	db.LinkToCollection("user")
	var t timelineRequest
	var u model.User
	json.NewDecoder(r.Body).Decode(&t)
	db.Get("email",t.User, &u)
	t.Timeline.User_ID = u.ID
	db.LinkToCollection("timeline")
	id := db.Insert(t.Timeline)
	db.LinkToCollection("user")
	find := make(map[string]interface{})
	find["identity"] = "_id"
	find["val"] = u.ID
	fmt.Println(u.ID)
	val := make(map[string]interface{})
	if len(u.Timelines) > 0 {

		val["timelines"] = id.(primitive.ObjectID)

		err := db.AddaDocument(find, val)
		if err != nil {
			fmt.Println(err)
		}
		res.Success("successfully added timeline", t)
	} else {
		u.Timelines = make([]primitive.ObjectID, 0)
		u.Timelines = append(u.Meetups, id.(primitive.ObjectID))

		val["timelines"] = u.Timelines
		db.UpdateaDocument(find, val)
		res.Success("successfully added meettup", t)
	}
	json.NewEncoder(w).Encode(res)

}

func GetAllTimeline(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-type", "application/json")
	db.LinkToCollection("user")
	var u model.User
	vars:=mux.Vars(r)
	id,_:=primitive.ObjectIDFromHex(vars["id"])
	db.Get("_id",id, &u)
	fmt.Println(u)
	timelines := db.GetAllTimelineofUser(&u)
	fmt.Println(timelines)
	res.Success("success for et all meetup", timelines)
	json.NewEncoder(w).Encode(res)
}

// func UpdateAfieldofTimeline(w http.ResponseWriter, r *http.Request) {
// 	w.Header().Add("Content-type", "application/json")
// 	db.LinkToCollection("meetup")
// 	var q Query
// 	json.NewDecoder(r.Body).Decode(&q)
// 	_, err := db.UpdateaDocument(q.Where.(map[string]interface{}), q.Val.(map[string]interface{}))
// 	if err != nil {
// 		res.ServerError("ERROR in Updating:->"+err.Error(), nil)
// 	}
// 	res.Success("Meetup Update successfully", q)
// 	json.NewEncoder(w).Encode(res)

// }
