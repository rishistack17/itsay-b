package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/rishi-org-stack/ItsaboutYou-b/auth"
	"github.com/rishi-org-stack/ItsaboutYou-b/model"
	"github.com/rishi-org-stack/ItsaboutYou-b/response"
	"go.mongodb.org/mongo-driver/bson/primitive"
	// "golang.org/x/tools/go/analysis/passes/nilfunc"
)

//TODO:Login authentican pai work kro
type Query struct {
	Where interface{}
	Val   interface{}
}

var store = sessions.NewCookieStore([]byte(os.Getenv("SESSION_KEY")))

const (
	database = "ITSABOUTYOU"
)

var db = model.Instantiate().Connect().CreateDb(database).LinkToCollection("user")
var res = &response.Response{}

func Shout(w http.ResponseWriter, r *http.Request) {

	val := make(map[string]string)
	val["weather"] = "is great"
	json.NewEncoder(w).Encode(val)
}

func Get(w http.ResponseWriter, r *http.Request) {
	// session, _ := store.Get(r, "session")

	// t, _ := session.Values["val"]
	// if okk{
	// fmt.Fprintf(w, "%v", t)
	// }
	// if !okk{
	// 	fmt.Fprintf(w,"%v","nootok ")
	// }

}
func Register(w http.ResponseWriter, r *http.Request) {
	inter := make(map[string]string)
	w.Header().Add("content-type", "application/json")
	var p model.User
	err := json.NewDecoder(r.Body).Decode(&inter)
	if err != nil {
		res.ServerError(err.Error(), r.Body)
	}
	p.Email = inter["email"]
	p.Name = inter["name"]
	var intPAss [60]byte
	copy(intPAss[:],inter["password"])
	p.Password = intPAss
	fmt.Println("ok yaha")
	fmt.Println(inter["password"])
	if db.UserIsalreadyRegistered(p.Email) {
		res.AlreadyPresent("User is already registerd", nil)

	} else {
		copy(intPAss[:],auth.GenrateHash(string(p.Password[:])))
		p.Password = intPAss

		p.DiaryPassword = auth.GenrateHash(p.DiaryPassword)
		// p.ID = id.(primitive.ObjectID)
		p.Meetups = make([]primitive.ObjectID, 0)
		p.Notes = make([]primitive.ObjectID, 0)
		p.Tasks = make(map[string]bool)
		p.Contacts = make([]primitive.ObjectID, 0)
		p.Timelines = make([]primitive.ObjectID, 0)
		db.Insert(p)
		res.Success("User is Successfully registered", p)
		res.Extra, _ = auth.CreateToken(p.ID.Hex(), string(p.Password[:]))
	}
	json.NewEncoder(w).Encode(res)
}

func Login(w http.ResponseWriter, r *http.Request) {
	inter := make(map[string]string)
	w.Header().Add("content-type", "json")
	var q model.User
	var p model.User
	json.NewDecoder(r.Body).Decode(&inter)
	q.Email = inter["email"]
	var intPAss [60]byte
	copy(intPAss[:],inter["password"])
	q.Password = intPAss
	if q.Email != "" || len(q.Password) != 0 {
		err := db.Get("email", q.Email, &p)
		fmt.Println(p.Password)
		fmt.Println(q.Password)
		if err != nil {
			res.NosuchDoc(err.Error(), nil)
		}
		same := auth.CompareHashes((p.Password[:]), (q.Password[:]))
		if !same {
			res.InvalidCrediantials("password doesnt matches", nil)
		}
		if same {
			rs := make(map[string]interface{})
			rs["id"] = p.ID
			rs["token"], _ = auth.CreateToken(p.ID.Hex(), string(p.Password[:]))
			res.Success("ok password matched", rs)
		}
	} else {
		res.ServerError("all data is not given", nil)

	}

	json.NewEncoder(w).Encode(res)
}

func GetaUser(w http.ResponseWriter, r *http.Request) {
	res.Method = r.Method
	w.Header().Add("content-type", "json")
	var u model.User
	vars := mux.Vars(r)
	id, _ := primitive.ObjectIDFromHex(vars["id"])
	db.Get("_id",id,&u)
	res.Success("",u)
	json.NewEncoder(w).Encode(res)
}

func UpdateUser(w http.ResponseWriter, r *http.Request) { 
	res.Method = r.Method
	w.Header().Add("content-type", "json")
	var q Query
	json.NewDecoder(r.Body).Decode(&q)

	c, err := db.UpdateaDocument(q.Where.(map[string]interface{}), q.Val.(map[string]interface{}))
	if err != nil {
		res.ErrorUpdateDoc(err.Error(), c)
	}
	res.Success("Update succesfull", q)

	json.NewEncoder(w).Encode(res)
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("content-type", "json")
	var q Query
	json.NewDecoder(r.Body).Decode(&q)
	count, err := db.DeleteADocument(q.Where.(map[string]interface{}))
	if err != nil {
		res.ErrorDeleteDoc(err.Error(), count)
	}
	res.Success("Delete succesfull", q)
	json.NewEncoder(w).Encode(res)
}

func Logout(w http.ResponseWriter, r *http.Request) {
	db.LinkToCollection("tokens")
	locate := make(map[string]interface{})
	locate["identity"] = "token"
	locate["val"] = r.Header.Get("Authorization")
	db.DeleteADocument(locate)
}

//TODO:Handle  token expiration
