package main
import (
   "fmt"
   "log"
   "net/http"
   "time"
   "github.com/joho/godotenv"
   ser "github.com/rishi-org-stack/ItsaboutYou-b/server"
   "github.com/rs/cors"
)

//TODO:Major poble occured update queries are not working on any route
//FIX IT --------RISHI.......FIX IT
//RISHI:Noticed doesnt works locally but not on docker 
func main() {
   err := godotenv.Load(".env")
   if err != nil {
      return 
   }
   router := ser.Route()
   c := cors.New(cors.Options{
      AllowedOrigins:   []string{"http://localhost:8081"},
      AllowCredentials: true,
   })
   srv := &http.Server{
      Handler: c.Handler(router),
      Addr:    ":8080",
      // Good practice: enforce timeouts for servers you create!
      WriteTimeout: 15 * time.Second,
      ReadTimeout:  15 * time.Second,
   }
   fmt.Println("Running on 8080")
   log.Fatal(srv.ListenAndServe())
}