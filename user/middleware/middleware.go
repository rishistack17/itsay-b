package middleware

import (
	"encoding/json"
	"fmt"
	"net/http"
	"github.com/rishi-org-stack/ItsaboutYou-b/auth"
	"github.com/rishi-org-stack/ItsaboutYou-b/model"
	"github.com/rishi-org-stack/ItsaboutYou-b/response"
	"go.mongodb.org/mongo-driver/bson/primitive"
)
const (
	database = "ITSABOUTYOU"
)
var res = &response.Response{}
var db = model.Instantiate().Connect().CreateDb(database).LinkToCollection("user")

func InputCheck(f http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var q model.User
		json.NewDecoder(r.Body).Decode(&q)
		if q.Email != "" || len(q.Password) != 0 {
			f.ServeHTTP(w, r)
		} else {
			res.ServerError("all data is not given", nil)
			// json.NewEncoder(w).Encode(res)
			fmt.Println(q.Email)

		}

	})
}

func Authenticate(f http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("Authorization")
		tk, is := auth.VerifyToken(token)
		if is != nil {
			res.ServerError("token is unvalidated", nil)
		}
		data := auth.ExtractPayload(tk)
		id, _ := primitive.ObjectIDFromHex(data["user_id"].(string))
		var z model.User
		db.Get("_id", id, &z)
		if z.IsSame(data["password"].(string)) {
			fmt.Println("ok authenticated ")
			f.ServeHTTP(w, r)
		}
		
	})
}
