package model

import (
	"context"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Meetup struct {
	ID      primitive.ObjectID `bson:"_id,omitempty"`
	With    Contact            `bson:"with"`
	Where   string             `bson:"where"`
	Subject []byte             `bson:"subject"`
	When    string             `bson:"when"`
	User_ID primitive.ObjectID `bson:"user_id" json:"user_id"`
}

func (db *DB) GetAllMeetup() []Meetup {
	ctx, cancel := context.WithTimeout(context.Background(), 50*time.Second)
	cursor, err := db.Collection.Find(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	var objects []Meetup
	if err = cursor.All(ctx, &objects); err != nil {
		log.Fatal(err)
	}

	defer cancel()
	return objects
}
