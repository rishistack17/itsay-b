package model

import "go.mongodb.org/mongo-driver/bson/primitive"

// import "time"

type Note struct {
	ID      primitive.ObjectID `bson:"_id,omitempty"`
	Day     string             `bson:"day"`
	Subject []string           `bson:"subject"`
	User_ID primitive.ObjectID `bson:"user_id" json:"user_id"`
}
