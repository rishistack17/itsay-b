package model

import (
	"context"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Task struct {
	ID          primitive.ObjectID `bson:"_id,omitempty"`
	DueDate     string             `bson:"duedate" json:"duedate"`
	Subject     string             `bson:"subject" json:"subject"`
	Description string             `bson:"description" json:"description"`
	Priority    int                `bson:"priority" json:"priority"`
	User_ID     primitive.ObjectID `bson:"user_id" json:"user_id"`
}

func (db *DB) GetAllTask() []Task {
	ctx, cancel := context.WithTimeout(context.Background(), 50*time.Second)
	cursor, err := db.Collection.Find(ctx, bson.M{})
	if err != nil {
		log.Fatal(err.Error())
	}
	var objects []Task
	if err = cursor.All(ctx, &objects); err != nil {
		log.Fatal(err)
	}

	defer cancel()
	return objects
}
