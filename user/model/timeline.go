package model

import (
	"context"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Timeline struct {
	ID                 primitive.ObjectID   `bson:"_id,omitempty"`
	TotalSavings       float64              `bson:"total_saving" json:"total_saving"`
	TotalEarning       float64              `bson:"total_earning" json:"total_earning"`
	NoofTasksCompleted int64                `bson:"no_of_tasks_completed" json:"no_of_tasks_completed"`
	Tasks              []Task               `bson:"tasks" json:"tasks"`
	TasksCompleted     []Task               `bson:"tasks_completed" json:"tasks_completed"`
	SubTimeline        []primitive.ObjectID `bson:"timelines" json:"timelines"`
	User_ID            primitive.ObjectID   `bson:"user_id" json:"user_id"`
}

func (db *DB) GetAllTimeline() []Timeline {
	ctx, cancel := context.WithTimeout(context.Background(), 50*time.Second)
	cursor, err := db.Collection.Find(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	var objects []Timeline
	if err = cursor.All(ctx, &objects); err != nil {
		log.Fatal(err)
	}

	defer cancel()
	return objects
}
