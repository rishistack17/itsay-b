package model

import (
	"context"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Hash [60]byte
type User struct {
	ID            primitive.ObjectID   `bson:"_id,omitempty"`
	Name          string               `bson:"name" json:"name"`
	Email         string               `bson:"email" json:"email"`
	Password      Hash                 `bson:"password" json:"password"`
	DiaryPassword string               `bson:"diary_password" json:"diary_password"`
	Age           int                  `bson:"age"    json:"age"`
	Gender        string               `bson:"gender" json:"gender"`
	Tasks         map[string]bool      `bson:"tasks"  json:"tasks"`
	Timelines     []primitive.ObjectID `bson:"timelines" json:"timelines"`
	Contacts      []primitive.ObjectID `bson:"contacts" json:"contacts"`
	Meetups       []primitive.ObjectID `bson:"meetups" json:"meetups"`
	Notes         []primitive.ObjectID `bson:"notes" json:"notes"`
}

//TODO:add is valid user
//TODO:genrate unique from name

func (db *DB) GetAllUser() []User {
	ctx, cancel := context.WithTimeout(context.Background(), 50*time.Second)
	cursor, err := db.Collection.Find(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	var objects []User
	if err = cursor.All(ctx, &objects); err != nil {
		log.Fatal(err)
	}
	defer cancel()
	return objects
}

func (db *DB) UserIsalreadyRegistered(email string) bool {
	res := false
	if len(db.GetAllUser()) < 1 {
		res = false
	} else {
		for _, val := range db.GetAllUser() {
			if val.Email == email {
				res = true
				break
			}
		}
	}
	return res
}

func (U *User) IsSame(pass string) (res bool) {
	res = false
	if string(U.Password[:]) == pass {
		res = true
	}

	return
}

func (db *DB) GetAllTaskofUser(u *User) []Task {
	var res []Task
	var task Task
	if len(u.Tasks) > 0 {
		db.LinkToCollection("task")
		for key := range u.Tasks {
			id, _ := primitive.ObjectIDFromHex(key)
			db.Get("_id", id, &task)
			res = append(res, task)
		}
	}

	return res
}

func (db *DB) GetAllContactsofUser(u *User) []Contact {
	var res []Contact
	var task Contact
	if len(u.Contacts) > 0 {
		db.LinkToCollection("contact")
		for _, key := range u.Contacts {
			// id,_ := primitive.ObjectIDFromHex(key)
			db.Get("_id", key, &task)
			res = append(res, task)
		}
	}

	return res
}

func (db *DB) GetAllmeetupsofUser(u *User) []Meetup {
	var res []Meetup
	var task Meetup
	if len(u.Meetups) > 0 {
		db.LinkToCollection("meetup")
		for _, key := range u.Meetups {
			// id,_ := primitive.ObjectIDFromHex(key)
			db.Get("_id", key, &task)
			res = append(res, task)
		}
	}

	return res
}
func (db *DB) GetAllTimelineofUser(u *User) []Timeline {
	var res []Timeline
	var timelines Timeline
	if len(u.Timelines) > 0 {
		db.LinkToCollection("timeline")
		for _, key := range u.Timelines {
			// id,_ := primitive.ObjectIDFromHex(key)
			db.Get("_id", key, &timelines)
			res = append(res, timelines)
		}
	}

	return res
}
