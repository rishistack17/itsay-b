package server

import (
	"net/http"

	"github.com/gorilla/mux"
	hnd "github.com/rishi-org-stack/ItsaboutYou-b/handler"
	mid "github.com/rishi-org-stack/ItsaboutYou-b/middleware"
)

func Route() *mux.Router {
	r := mux.NewRouter().StrictSlash(true)
	r.HandleFunc("/", (hnd.Shout)).Methods("GET")
	r.HandleFunc("/ok", func(w http.ResponseWriter, requet *http.Request) {

	})
	r.HandleFunc("/", (hnd.Shout)).Methods("POST", http.MethodPatch)

	r.HandleFunc("/s", hnd.Get).Methods("GET", "POST")

	userRoute := r.PathPrefix("/user").Subrouter()
	Taskroute := r.PathPrefix("/task").Subrouter()
	contactRoute := r.PathPrefix("/contact").Subrouter()
	meetupRoute := r.PathPrefix("/meetup").Subrouter()
	timelineRoute := r.PathPrefix("/timeline").Subrouter()
	admin := r.PathPrefix("/admin").Subrouter()
	admin.HandleFunc("/", hnd.Shout)

	//USER  ROUTES STARTS HERE
	userRoute.HandleFunc("/", hnd.Register).Methods("POST")                      //this will  be ued for registration
	userRoute.HandleFunc("/login", hnd.Login).Methods(http.MethodPost)                     //login route for user
	userRoute.HandleFunc("/{id}", mid.Authenticate(hnd.GetaUser)).Methods("GET") //this will be used for user profile page
	userRoute.HandleFunc("/update", mid.Authenticate(hnd.UpdateUser)).Methods("PUT")
	userRoute.HandleFunc("/delete", mid.Authenticate(hnd.DeleteUser)).Methods("DELETE")
	userRoute.HandleFunc("/logout", mid.Authenticate(hnd.Logout)).Methods("GET")

	//USER ROUTES END HERE

	//TASKS ROUTES START'S HERE 
	Taskroute.HandleFunc("/{id}", mid.Authenticate(hnd.AddTask)).Methods("POST")
	Taskroute.HandleFunc("/{id}", mid.Authenticate(hnd.GetAllTask)).Methods("GET")
	Taskroute.HandleFunc("/completed", mid.Authenticate(hnd.MarkCompleted)).Methods("POST")
	Taskroute.HandleFunc("/update", mid.Authenticate(hnd.UpdateAfield)).Methods("POST")
	Taskroute.HandleFunc("/delete", mid.Authenticate(hnd.DeleteTask)).Methods("DELETE")

	//TASKS ROUTE END HERE

	//contacts Route starts HERE
	contactRoute.HandleFunc("/", mid.Authenticate(hnd.AddConact)).Methods(http.MethodPost)
	contactRoute.HandleFunc("/{id}", mid.Authenticate(hnd.GetAllContact)).Methods(http.MethodGet)
	contactRoute.HandleFunc("/{id}", mid.Authenticate(hnd.UpdateAfieldofContact)).Methods(http.MethodPut)
	//#ENDS HERE
	meetupRoute.HandleFunc("/", mid.Authenticate(hnd.AddMeetup)).Methods(http.MethodPost)
	meetupRoute.HandleFunc("/{id}", mid.Authenticate(hnd.GetAllmeetup)).Methods(http.MethodGet)
	meetupRoute.HandleFunc("/{id}", mid.Authenticate(hnd.UpdateAfieldofmeetup)).Methods(http.MethodPut)
	timelineRoute.HandleFunc("/", hnd.Shout)
	timelineRoute.HandleFunc("/add", mid.Authenticate(hnd.AddTimeline))
	timelineRoute.HandleFunc("/{id}", mid.Authenticate(hnd.GetAllTimeline))
	return r
}
